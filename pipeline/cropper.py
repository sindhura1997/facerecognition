#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import numpy as np
import random
import time
from PIL import Image
import pandas as pd
import errno
import yaml
import facealigner

with open("./config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def crop():
	
	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cfg['cnn_face_detector_path'])

	# Initialize dlib shape predictor
	shape_predictor = dlib.shape_predictor(cfg['predictor_path'])

	# Detection model type
	detection_model = cfg['face_detector_model'] # or 'cnn'
	
	UPSAMPLE_FACTOR = 1
	margin = 10

	dirs=os.listdir(cfg['FRAMES_AUG_DIR'])
	for dir in dirs:
		try:
			if not os.path.exists(cfg['CROP_DIR']+'/'+dir):
				os.makedirs(cfg['CROP_DIR']+'/'+dir)
		except OSError as e:
			if e.errno != errno.EEXIST:
				print('Cropper Line 42: ', e)
				return False
			else:
				pass
					
		for image_name in os.listdir(cfg['FRAMES_AUG_DIR']+'/'+dir):
			image_path = cfg['FRAMES_AUG_DIR']+'/'+dir+'/'+image_name
			try:
				image = cv2.imread(image_path)
				gray_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
				if detection_model == 'cnn':
					detector = cnn_face_detector	

				dets = face_detection(detector, image, UPSAMPLE_FACTOR)
				# Now process each face we found.
				face_encodings = []
				for k, d in enumerate(dets):
						
					# if detection_model == 'cnn':
					# 	left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
					# else:
					# 	left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
					
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()	
					cropped_img = image[top-margin:bottom+margin, left-margin:right+margin]
					# height, width = cropped_img.shape[:2]
					# top = max(0, top)
					# left = max(0, left)
					# right = min(width, right)
					# bottom = min(bottom, right)
					# dd = dlib.rectangle(left, top, right, bottom)
					
					# Align Faces and save
					aligned_face = facealigner.align(shape_predictor, image, gray_img, d)
					cv2.imwrite(cfg['CROP_DIR']+'/'+dir+'/'+dir.split('_')[0]+image_name, aligned_face)
				
			except Exception as e:
				print('Cropper Line 84: ',e)
				#continue
	return True

if __name__ == '__main__':
	crop()

