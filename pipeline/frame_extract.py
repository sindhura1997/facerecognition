import os,cv2,errno
from datetime import datetime
import yaml
import numpy as np

def rotate_bound(image, angle):
	# grab the dimensions of the image and then determine the
	# center
	(h, w) = image.shape[:2]
	(cX, cY) = (w // 2, h // 2)

	# grab the rotation matrix (applying the negative of the
	# angle to rotate clockwise), then grab the sine and cosine
	# (i.e., the rotation components of the matrix)
	M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
	cos = np.abs(M[0, 0])
	sin = np.abs(M[0, 1])

	# compute the new bounding dimensions of the image
	nW = int((h * sin) + (w * cos))
	nH = int((h * cos) + (w * sin))
 
	# adjust the rotation matrix to take into account translation
	M[0, 2] += (nW / 2) - cX
	M[1, 2] += (nH / 2) - cY
 
	# perform the actual rotation and return the image

	return cv2.warpAffine(image, M, (nW, nH))

with open("./config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

def make_framedir(dir_name):
	sp = os.path.join(cfg['FRAMES_DIR']+dir_name)
	try:
		os.makedirs(sp)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass
	return sp

def save_data(video_dir, fname, spath):
	# print(fname,spath)
	n_frames = 800
	cap = cv2.VideoCapture(os.path.join(video_dir+fname))
	# print(VIDEOS_DIR+'/'+fname)
	# print(os.path.join(video_dir+fname))
	if cap.isOpened() == False:
		return False
	try:
		num = 0
		ret=True
		while (ret) and (num<n_frames):
			ret, frame = cap.read()
			frame1 = rotate_bound(frame, -270)
			if ret and num%7==0:
				t = str(datetime.now())
				t = t.split(' ')
				t = '_'.join(t)

				cv2.imwrite(spath+'/{}frame{}.jpg'.format(t, num), frame1)
			num=num+1
				
		cap.release()
		cv2.destroyAllWindows()
		return True
	except:
		return False

def main(video_dir):
	dir_names=os.listdir(video_dir)
	for file in dir_names:
		if file!='':
			fname, file_extension = os.path.splitext(file)
			sp=make_framedir(fname)
			if not save_data(video_dir, file, sp):
				return {'msg': 'frame extraction failed','status':400}
		else:
			continue

if __name__=="__main__":
	main(cfg['INPUT_DIR'])