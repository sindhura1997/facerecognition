import os
import pandas as pd
import csv
import shutil
import yaml
import redis
import requests
import json
import redis
import unicodedata
import pymysql


def move_inactive_employees(source_path, target_path):
	converge_api_endpoint = 'https://converge.namely.com/api/v1/reports{0}'
	redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)
	active_url = converge_api_endpoint.format('/de54bc2c-daf1-4d26-908d-734899b79597')
	active_mumbai=converge_api_endpoint.format('/ef626569-e636-4030-abd6-6421d17c8ba8')
	inactive_url= converge_api_endpoint.format('/0519c108-b065-4b17-a203-f20274fa9088')
	
	# List all people whose data is captured
	all_existing_emps = os.listdir(source_path)
	all_existing_emp = [each_dir.split('_')[0].lower() for each_dir in all_existing_emps]
	#print(all_existing_emp)

	# Get all active employees from Mumbai location from FMS api
	headers = { 'ID' : 'de54bc2c-daf1-4d26-908d-734899b79597',
			  'Authorization' : 'Bearer o9rAPeLCcLnhc8HLVShLJNB9mIL1mvgx3vnHnnftuHDqIL0jZDRo1K5DdSRUnXTs',
			  'Accept' : 'application/json'}

	r = requests.get(active_mumbai, headers = headers)
	reports=r.json()['reports']

	active_emp_list = ''
	if (r.status_code == 200):
		for i in reports:
			active_emp_list = [item for item in i['content']]
	else:
		return "{0}: {1}".format(r.status_code, r.text)

	with open('emp_without_trainingdata.csv', 'a') as csvFile:
		writer = csv.writer(csvFile)
		for each_active_emp in active_emp_list:
			try:
				if each_active_emp[1].lower() not in all_existing_emp:
					writer.writerow(each_active_emp[1:])
					#print(each_active_emp[1].lower())
					pass
			except Exception as e:
				#print(each_active_emp, e)
				continue

if __name__=="__main__":
	with open("./config.yml", 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

	#source_path = cfg['CROP_DIR']
	source_path = "/data/Facerecognition/facerec_dev/pipeline/archive/Fractal/Mum/cropped_archive/"
	target_path = cfg['INACTIVE_EMPLOYEES_DIR']
	move_inactive_employees(source_path, target_path)