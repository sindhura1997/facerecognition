import os
import pandas as pd
import csv
import shutil
import yaml
import redis
import requests
import json
import redis
import unicodedata
import pymysql


def update_mysql_db(employee_id):

	# Connect to the database
	connection = pymysql.connect(host='localhost',
							 user=cfg['sqlusr'],
							 password=cfg['sqlpwd'],
							 db=cfg['sqldb'],
							 charset='utf8mb4',
							 cursorclass=pymysql.cursors.DictCursor)
	try:
		# Open connevction and update database table
		with connection.cursor() as cursor:
			sql_query = "Update encoding_table set active='N' where label = %s"
			#sql_query = 'select label, active from encoding_table where label = %s'
			cursor.execute(sql_query, (employee_id, ))

			# connection is not autocommit by default. So you must commit to save the changes.
			connection.commit()
			print('Moved inactive {}'.format(employee_id))
	finally:
		connection.close()

def clear_keys_pat(pattern, redis_db):
	keys =[o for o in redis_db.scan_iter(pattern)]
	if keys:
		for i in keys:
			redis_db.delete(i)

def move_inactive_employees(source_path, target_path):
	converge_api_endpoint = 'https://converge.namely.com/api/v1/reports{0}'
	redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)
	active_url = converge_api_endpoint.format('/de54bc2c-daf1-4d26-908d-734899b79597')
	active_mumbai=converge_api_endpoint.format('/ef626569-e636-4030-abd6-6421d17c8ba8')
	inactive_url= converge_api_endpoint.format('/0519c108-b065-4b17-a203-f20274fa9088')
	#clear_keys_pat("fms_*", redis_db)

	headers = { 'ID' : 'de54bc2c-daf1-4d26-908d-734899b79597',
			  'Authorization' : 'Bearer o9rAPeLCcLnhc8HLVShLJNB9mIL1mvgx3vnHnnftuHDqIL0jZDRo1K5DdSRUnXTs',
			  'Accept' : 'application/json'}

	r = requests.get(active_mumbai, headers = headers)
	reports=r.json()['reports']

	active_emp_list = ''
	if (r.status_code == 200):
		for i in reports:
			active_emp_list = [item[1] for item in i['content']]

		# Optional - Write into CSV
		# with open("active_emp_mumbai.csv", "w+") as f:
		# 	writer = csv.writer(f)
		# 	list_columns = "Action_Inactive,Empid,Name,Email,DOJ,Location,Designation".split(',')
		# 	writer.writerow(list_columns)
		# 	for each_line in i['content']:
		# 		writer.writerow(each_line)

	else:
		return "{0}: {1}".format(r.status_code, r.text)

	all_existing_emps = os.listdir(source_path)
	all_existing_emp = [each_dir for each_dir in all_existing_emps]

	for each in all_existing_emp:
		#print('Processing: {}'.format(each))
		if each.lower().startswith('vm'):
			continue
		if each.strip().split('_')[0] not in active_emp_list:
			if os.path.exists(target_path+each):
				shutil.rmtree(target_path+each)
			shutil.move(source_path+each, target_path)

			# Once you find the inactive; update the mysql embedings with active flag as 0
			update_mysql_db(each)


if __name__=="__main__":
	with open("../config.yml", 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

	source_path = '/data/Facerecognition/facerec_dev/pipeline/archive/Fractal/Mum/cropped_archive/'
	target_path = '/data/Facerecognition/facerec_dev/pipeline/archive/Fractal/InactiveEmployees/'
	move_inactive_employees(source_path, target_path)