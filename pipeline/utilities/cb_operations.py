import xmltodict, json, sys,os
from couchbase import Couchbase
from couchbase.cluster import Cluster
from couchbase.cluster import PasswordAuthenticator
from couchbase.bucket import Bucket
from couchbase.exceptions import CouchbaseError
from couchbase.exceptions import CouchbaseTransientError
import base64
from datetime import datetime
import time
from couchbase.n1ql import N1QLQuery
import cv2
import dlib

class CBOperations:
	def __init__(self, cb_server, cb_username, cb_password, cb_bucket_name, cb_bucket_timeout, image_source, facerec_location, image_type, cropped_save_path, cropped_load_path, prediction_model_path, recognition_model_path):
		self.cb_server = "couchbase://localhost:8091/"
		self.cb_username = "administrator"
		self.cb_password = "administrator"
		self.cb_bucket_name = "fractal"
		self.cb_bucket_timeout = 200
		self.image_source = "FractalEmployee"
		self.shape_predictor = dlib.shape_predictor("/data/Facerecognition/models/shape_predictor_68_face_landmarks.dat")
		self.facerec_model = dlib.face_recognition_model_v1("/data/Facerecognition/models/dlib_face_recognition_resnet_model_v1.dat")
		self.input_dim = 128

		'''
		cb_server: couchbase://localhost:8091/
		cb_username: 'administrator'
		cb_password: 'administrator'
		cb_bucket_name: facedb
		cb_bucket_timeout: 200
		image_source: FractalEmployee
		facerec_location: 4 # {1: Mumbai, 2: Banglore, 3: Gurugram, 4:NY (also for all locations)}
		image_type: FaceRecognition
		'''

	def facerec_embedding(self, cropped_image):	
		try:
			width, height, _ = cropped_image.shape
			dd = dlib.rectangle(0, 0, width, height)
			shape = self.shape_predictor(cropped_image, dd)			

			# Compute face encoding (128D vectors)
			face_descriptor = list(self.facerec_model.compute_face_descriptor(cropped_image, shape))
			return face_descriptor
		except Exception as e:
			print(e)
			return None

	def InsertEmbeddingsIntoCB(self):

		cluster = Cluster(self.cb_server)
		authenticator = PasswordAuthenticator(self.cb_username, self.cb_password)
		cluster.authenticate(authenticator)
		cb_bucket = cluster.open_bucket(self.cb_bucket_name)
		cb_bucket.timeout = self.cb_bucket_timeout

		#bucket_name = Bucket(self.cb_server+self.bucket_name)
		#dir_path = self.cropped_save_path # 'image_path with / at the end'
		
		for image_dir in os.listdir(dir_path):
			image_dirpath = dir_path+image_dir				
			print('Processing: ', image_dirpath)

			img_batch = dict()
			for image_name in os.listdir(image_dirpath):
				image_path = image_dirpath+'/'+image_name
				
				try:
					cropped_image = cv2.imread(image_path) 
					image_encodings = self.facerec_embedding(cropped_image)
					if image_encodings is None:
						print("==== None ====")
						continue

					image_data = dict()
					#with open(image_path, 'rb') as f:
					#	image = f.read()

					image_data['emp_id'] = image_dir.split('_')[0]
					image_data['name'] = image_dir.split('_')[1] +'_'+image_dir.split('_')[2]
					image_data['location'] = '4' # 1. Mumbai 2. Banglore 3. Gurugram 4. NYC
					image_data['image_name'] = image_name 
					image_data['path'] = image_path 
					image_data['image_id'] = image_data['emp_id']+'_'+image_name
					image_data['source'] = self.image_source
					image_data['image_encodings'] = image_encodings
					image_data['active'] = 'Y'
					#image_data['data'] = image.encode('base64')

					img_batch[image_dir.split('_')[0]+'_'+image_name]=image_data
				except Exception as e:
					print("Exception", e)
					continue


			try:
				cb_bucket.upsert_multi(img_batch)
			except CouchbaseError as exc:
				for k, res in exc.all_results.items():
					if res.success:
						# Handle successful operation
						print('Inserted sucessfully!')
					else:
						print("Key {0} failed with error code {1}".format(k, res.rc))
						print("Exception {0} would have been thrown".format(CouchbaseError.rc_to_exctype(res.rc)))

	def ReadEmbeddingsIntoCB(self):
		cluster = Cluster(self.cb_server)
		authenticator = PasswordAuthenticator(self.username, self.password)
		cluster.authenticate(authenticator)
		cb_bucket = cluster.open_bucket(self.bucket_name)
		cb_bucket.timeout = self.bucket_timeout

		row_iter = cb_bucket.n1ql_query(N1QLQuery(self.n1ql_query))
		for row in row_iter: 
			save_dir = self.save_path +row['emp_id']+'_'+row['name']
			
			if not os.path.exists(save_dir):
				os.mkdir(save_dir)

			with open(save_dir+'/'+row['image_id'], 'wb') as f:
				f.write(base64.b64decode(row['data']))	

# ================================================================================= #