#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import numpy as np
import random
import time
import datetime
from PIL import Image
import pandas as pd
import errno
import csv
import pandas as pd
from sqlalchemy import create_engine
import yaml

from couchbase import Couchbase
from couchbase.cluster import Cluster
from couchbase.cluster import PasswordAuthenticator
from couchbase.bucket import Bucket
from couchbase.exceptions import CouchbaseError
from couchbase.exceptions import CouchbaseTransientError

from couchbase.n1ql import N1QLQuery
from datetime import datetime
import hashlib

with open("../config.yml", 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

def insertEmbeddingsIntoCB(df):
		
	cluster = Cluster(cfg['cb_server'])
	authenticator = PasswordAuthenticator(cfg['cb_username'], cfg['cb_password'])
	cluster.authenticate(authenticator)
	cb_bucket = cluster.open_bucket('facerec_'+cfg['training_model_type'])
	cb_bucket.timeout = cfg['cb_bucket_timeout']

	df_size = len(df)
	img_batch = dict()
	for index, row in df.iterrows():
		try:
			# Create the json document
			image_data = dict()
			image_data['label'] = row['label']
			image_data['location'] = '4' # 1. Mumbai 2. Banglore 3. Gurugram 4. NYC
			image_data['image_name'] = row['image_path'] 
			image_data['image_encodings'] = [row['encode_'+str(i)] for i in range(1, 129)]
			image_data['active'] = row['active']
			image_data['insert_time'] = datetime.now().strftime("%Y-%m-%d %H:%M")

			# Create the batch document for upload
			doc_id = "_".join([str(row['encode_'+str(i)]) for i in range(1, 129)])
			doc_id = row['label']+'_'+doc_id			
			img_batch[hashlib.sha1(doc_id.encode('utf-8')).hexdigest()]=image_data

			if (index>0) and (index%cfg['cb_batchsize'] == 0 or index==df_size-1):
				print('Inserting Batch for : ', index, df_size)
				try:
					cb_bucket.upsert_multi(img_batch)
					#cb_bucket_all.upsert_multi(img_batch)
					img_batch = dict()
				except CouchbaseError as exc:
					for k, res in exc.all_results.items():
						if res.success:
							# Handle successful operation
							print('Inserted sucessfully!')
						else:
							print("Key {0} failed with error code {1}".format(k, res.rc))
							print("Exception {0} would have been thrown".format(CouchbaseError.rc_to_exctype(res.rc)))

		except Exception as e:
			print("Exception", e)
			continue

def read_encodings():
	con_string='mysql+pymysql://'+cfg['sqlusr']+':'+cfg['sqlpwd']+'@localhost/'+cfg['sqldb']
	con=create_engine(con_string)
	try:
		if cfg['training_model_type'] == 'fractal':
			print('Reading for Fractal Encoding Table')
			query=("SELECT * from encoding_table")
			embeddings=pd.read_sql(query, con=con)
		elif cfg['training_model_type'] == 'other':
			print('Reading for Indian Encoding Table')
			query=("SELECT * from encoding_table_other")
			embeddings=pd.read_sql(query, con=con)
		else:		
			print('Reading for Combined Encoding Table')
			query=("SELECT * from encoding_table_all")
			embeddings=pd.read_sql(query, con=con)

	except Exception as e:
		print(e)
	
	embeddings = embeddings.drop(['insert_time'],  axis=1)
	return embeddings


if __name__=="__main__":
	embeddings = read_encodings()
	insertEmbeddingsIntoCB(embeddings)