import yaml
from cb_operations import CBOperations

with open("../config.yml", 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

cbinsert = CBOperations(cfg['cb_server'], cfg['cb_username'], cfg['cb_password'], cfg['cb_bucket_name'], cfg['cb_bucket_timeout'], cfg['image_source'], 
	cfg['facerec_location'], cfg['image_type'], cfg['cropped_save_path'], cfg['cropped_load_path'], cfg['dl_shape_predictor_path'], cfg['dl_face_recognition_path'])
cbinsert.InsertFaceRecIntoCB()
