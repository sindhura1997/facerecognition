import os,cv2,errno,subprocess
from datetime import datetime
import frame_extract
import img_augment
import cropper
#import cropper_mysql
import train_data_mysql
import yaml
import mysql_encodings

with open("./config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

def main():
	
	#print("1: Extraction Starts")
	#frame_extract.main(cfg['INPUT_DIR'])
	#print("Extraction Completed")

	print("2: Augmentation Starts")
	# img_augment.augment()
	print("Augmentation Completed")
	
	print("3: Cropping Starts")
	# cropper.crop()
	print("Cropping Completed")

	if cfg['training_model_type'] != 'all':
		print("4: Encoding Starts")
		mysql_encodings.main_encoder()
		print("Encoding Completed")

	print("5: Training Starts")
	train_data_mysql.train()
	print("Training Completed")

if __name__=="__main__":
	main()