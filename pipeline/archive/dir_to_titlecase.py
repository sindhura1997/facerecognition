import os
import sys
import shutil

dir_path = './facerec_image/'
tar_path = './facerec_image_title/'

all_dirs = os.listdir(dir_path)

for each_dir in all_dirs:
	titlecase_name = str.title(each_dir)

	#os.rename()
	#print(each_dir, titlecase_name)
	try:
		shutil.move(dir_path+each_dir, tar_path+titlecase_name)
	except Exception as e:
		print(each_dir, e)
		continue
