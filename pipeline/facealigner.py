import cv2
import numpy as np
import dlib
from collections import OrderedDict


# define a dictionary that maps the indexes of the facial landmarks to specific face regions
FACIAL_LANDMARKS_IDXS = OrderedDict([
	("mouth", (48, 68)),
	("right_eyebrow", (17, 22)),
	("left_eyebrow", (22, 27)),
	("right_eye", (36, 42)),
	("left_eye", (42, 48)),
	("nose", (27, 36)),
	("jaw", (0, 17))
])

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)

	# loop over the 68 facial landmarks and convert them to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)

	# return the list of (x, y)-coordinates
	return coords

def align(predictor, image, gray, rect):
	# convert the landmark (x, y)-coordinates to a NumPy array
	desiredLeftEye=(0.30, 0.30)
	desiredFaceWidth=256
	desiredFaceHeight=int(1.2* desiredFaceWidth)

	#if the desired face height is None, set it to be the desired face width (normal behavior)
	if desiredFaceHeight is None:
		desiredFaceHeight = desiredFaceWidth
	
	shape = predictor(gray, rect)
	shape = shape_to_np(shape)

	# extract the left and right eye (x, y)-coordinates
	(lStart, lEnd) = FACIAL_LANDMARKS_IDXS["left_eye"]
	(rStart, rEnd) = FACIAL_LANDMARKS_IDXS["right_eye"]
	leftEyePts = shape[lStart:lEnd]
	rightEyePts = shape[rStart:rEnd]

	# compute the center of mass for each eye
	leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
	rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

	# compute the angle between the eye centroids
	dY = rightEyeCenter[1] - leftEyeCenter[1]
	dX = rightEyeCenter[0] - leftEyeCenter[0]
	angle = np.degrees(np.arctan2(dY, dX)) - 180

	# compute the desired right eye x-coordinate based on the desired x-coordinate of the left eye
	desiredRightEyeX = 1.0 - desiredLeftEye[0]

	# determine the scale of the new resulting image by taking the ratio of the distance between eyes in the *current*
	# image to the ratio of distance between eyes in the *desired* image
	dist = np.sqrt((dX ** 2) + (dY ** 2))
	desiredDist = (desiredRightEyeX - desiredLeftEye[0])
	desiredDist *= desiredFaceWidth
	scale = desiredDist / dist

	# compute center (x, y)-coordinates (i.e., the median point) between the two eyes in the input image
	eyesCenter = ((leftEyeCenter[0] + rightEyeCenter[0]) // 2, (leftEyeCenter[1] + rightEyeCenter[1]) // 2)

	# grab the rotation matrix for rotating and scaling the face
	M = cv2.getRotationMatrix2D(eyesCenter, angle, scale)

	# update the translation component of the matrix
	tX = desiredFaceWidth * 0.5
	tY = desiredFaceHeight * desiredLeftEye[1]
	M[0, 2] += (tX - eyesCenter[0])
	M[1, 2] += (tY - eyesCenter[1])

	# apply the affine transformation
	(w, h) = (desiredFaceWidth, desiredFaceHeight)
	output = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC)

	# return the aligned face
	return output