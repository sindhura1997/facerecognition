import os
import sys
import glob
from scipy import misc
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import errno
import yaml

with open("./config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)


def make_dir(base_dir,dir_name):
	sp = base_dir+'/'+dir_name
	try:
		os.makedirs(sp)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass
	return sp

def augment():
	gen_images=cfg['aug_img_no']
	dirs=os.listdir(cfg['FRAMES_DIR'])
	spaths=[make_dir(cfg['FRAMES_AUG_DIR'],dir) for dir in dirs]

	datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.20,
        height_shift_range=0.15,
        shear_range=0.10,
        zoom_range=0.10,		
        fill_mode='nearest')

	for dir in dirs:
		no_of_images=len(glob.glob(os.path.join(cfg['FRAMES_DIR'],dir, "*")))
		print('Processing: ', dir)
		if no_of_images!=0:
			image_factor=int(gen_images)/int(no_of_images)
			for image in glob.glob(os.path.join(cfg['FRAMES_DIR'],dir,"*")):
				img = misc.imread(image) # this is a PIL image
				x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)
				x = x.reshape((1,) + x.shape)  # this is a Numpy array with shape (1, 3, 150, 150)
				# the .flow() command below generates batches of randomly transformed images
				i = 0
				for batch in datagen.flow(x, batch_size=1, save_to_dir=cfg['FRAMES_AUG_DIR']+'/'+dir, save_format='jpg'):
					i += 1	
					if i > image_factor:
						break  # otherwise the generator would loop indefinitely
					if len(glob.glob(os.path.join(cfg['FRAMES_AUG_DIR']+'/'+dir, "*"))) >=int(gen_images):
						print( 'limit reached')
						print( len(glob.glob(os.path.join(cfg['FRAMES_AUG_DIR']+'/'+dir, "*"))))
						break
				if len(glob.glob(os.path.join(cfg['FRAMES_AUG_DIR']+'/'+dir, "*"))) >=int(gen_images):
					# print( 'limit reached')
					# print( len(glob.glob(os.path.join(cfg['FRAMES_AUG_DIR']+'/'+dir, "*"))))
					break
	return True

if __name__=="__main__":
	augment()