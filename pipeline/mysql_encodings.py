#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import numpy as np
import random
import time
import datetime
from PIL import Image
import pandas as pd
import errno
import csv
import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime
import yaml
import hashlib

from couchbase import Couchbase
from couchbase.cluster import Cluster
from couchbase.cluster import PasswordAuthenticator
from couchbase.bucket import Bucket
from couchbase.exceptions import CouchbaseError
from couchbase.exceptions import CouchbaseTransientError

with open("./config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

def insertEmbeddingsIntoCB(df):
		
	input_dim = 128

	cluster = Cluster(cfg['cb_server'])
	authenticator = PasswordAuthenticator(cfg['cb_username'], cfg['cb_password'])
	cluster.authenticate(authenticator)
	cb_bucket = cluster.open_bucket('facerec_'+cfg['training_model_type'])
	cb_bucket.timeout = cfg['cb_bucket_timeout']

	cb_bucket_all = 'facerec_all'
	cb_bucket_all = cluster.open_bucket(cb_bucket_all)
	cb_bucket_all.timeout = cfg['cb_bucket_timeout']

	df_size = len(df)
	img_batch = dict()
	for index, row in df.iterrows():
		try:
			image_data = dict()
			#with open(image_path, 'rb') as f:
			#	image = f.read()

			image_data['label'] = row['label']
			image_data['location'] = '4' # 1. Mumbai 2. Banglore 3. Gurugram 4. NYC
			image_data['image_name'] = row['image_path'] 
			image_data['image_encodings'] = [row['encode_'+str(i)] for i in range(input_dim)]
			image_data['active'] = 'Y'
			image_data['insert_time'] = datetime.now().strftime("%Y-%m-%d %H:%M")

			#img_batch[row['label']+'_'+row['image_path'].split('.')[0]]=image_data
			# Create the batch document for upload
			doc_id = "_".join([str(row['encode_'+str(i)]) for i in range(1, 129)])
			doc_id = row['label']+'_'+doc_id			
			img_batch[hashlib.sha1(doc_id.encode('utf-8')).hexdigest()]=image_data

			if (index>0) and (index%cfg['cb_batchsize'] == 0 or index==df_size-1):
				print('Inserting Batch for : ', index+1, df_size)
				try:
					cb_bucket.upsert_multi(img_batch)
					cb_bucket_all.upsert_multi(img_batch)
					img_batch = dict()
				except CouchbaseError as exc:
					for k, res in exc.all_results.items():
						if res.success:
							# Handle successful operation
							print('Inserted sucessfully!')
						else:
							print("Key {0} failed with error code {1}".format(k, res.rc))
							print("Exception {0} would have been thrown".format(CouchbaseError.rc_to_exctype(res.rc)))

		except Exception as e:
			print("Exception", e)
			continue

def add_new_encoding(new_df):
	con_string='mysql+pymysql://'+cfg['sqlusr']+':'+cfg['sqlpwd']+'@localhost/'+cfg['sqldb']
	con=create_engine(con_string)
	#To make column names same as mysql table headers
	#Run below if column names are different for new_df and mysql table
	if cfg['training_model_type'] == 'fractal':
		q=("select column_name from information_schema.columns where table_name='encoding_table'")
	elif cfg['training_model_type'] == 'other':
		q=("select column_name from information_schema.columns where table_name='encoding_table_other'")
	else:
		q=("select column_name from information_schema.columns where table_name='encoding_table_all'")

	cols=pd.read_sql(q,con=con)
	new_cols = {x: y for x, y in zip(new_df.columns, cols['column_name'].tolist())}
	new_df=new_df.rename(columns=new_cols)
	indx=0
	total=len(new_df)
	#Add new_df data to mysql table
	while(indx<total):
		if indx+5000<total:
			lmt=indx+5000
		else:
			lmt=total
		with con.connect() as conn, conn.begin():
			#print(indx,lmt)
			#new_df.to_sql('encoding_table', conn, if_exists='append', index = False)
			if cfg['training_model_type'] == 'fractal':
				new_df.iloc[indx:lmt].to_sql('encoding_table', conn, if_exists='append', index = False)
			elif cfg['training_model_type'] == 'other':
				new_df.iloc[indx:lmt].to_sql('encoding_table_other', conn, if_exists='append', index = False)
			else:
				new_df.iloc[indx:lmt].to_sql('encoding_table_all', conn, if_exists='append', index = False)

		indx=lmt

def face_encoder():
	shape_predictor = dlib.shape_predictor(cfg['predictor_path'])
	facerec_model = dlib.face_recognition_model_v1(cfg['face_rec_model_path'])
	# know_face_encoding = []
	# known_faces = []
	column_list = ['encode_'+str(i) for i in range(128)]
	column_list.append('label')
	column_list.append('image_path')

	df = pd.DataFrame(columns=column_list)
	i = 0
	crop_dir = cfg['CROP_DIR']
	
	# crop_dir = ''
	# if cfg['training_model_type'] == 'fractal':
	# 	print("\nGenerating embeddings for Fractal Employess")
	# 	crop_dir = cfg['CROP_DIR']
	# elif cfg['training_model_type'] == 'other':
	# 	print("\nGenerating embeddings for Others")
	# 	crop_dir = cfg['CROP_DIR_OTHER']
	
	for dir in os.listdir(crop_dir):
		all_images = os.listdir(crop_dir+dir)
		for each_image in all_images:
			try:
				image_path  = crop_dir+dir+'/'+each_image
				image = cv2.imread(image_path)		
				height, width, _ = image.shape
				dd = dlib.rectangle(0, 0, width, height)
				shape = shape_predictor(image, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = list(facerec_model.compute_face_descriptor(image, shape))
				face_descriptor.append(dir)
				face_descriptor.append(each_image)	
				
				df.loc[i] = face_descriptor
				i += 1
			except:
				continue
	return df

def main_encoder():	
	new_encodings=face_encoder()
	# new_encodings.to_csv('face_encodings.csv', sep=',', encoding='utf-8')
	# add_new_encoding(new_encodings)
	insertEmbeddingsIntoCB(new_encodings)

if __name__ == '__main__':
	#main(sys.argv[1],sys.argv[2],sys.argv[3])
	main_encoder()	
