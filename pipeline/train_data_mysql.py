import pandas as pd
from datetime import datetime
import pickle
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sqlalchemy import create_engine
import yaml

from couchbase import Couchbase
from couchbase.cluster import Cluster
from couchbase.cluster import PasswordAuthenticator
from couchbase.bucket import Bucket
from couchbase.exceptions import CouchbaseError
from couchbase.exceptions import CouchbaseTransientError
from couchbase.n1ql import N1QLQuery

with open("./config.yml", 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

def train_model_fromCB(df, model_file):	
	print('Reading ML Model from CBEmbeddings')
	y = df['label']
	X = df.drop(['label'],  axis=1)
	##X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	##print("classes",len([name for name in set(y)]))
	svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	svm.fit(X, y)
	with open(model_file, 'wb') as fp:
		pickle.dump(svm, fp)

def readEmbeddingsFromCB():
	
	print('\nReading embeddings from CB')
	column_list = ['encode_'+str(i) for i in range(128)]
	column_list.append('label')
	#df = pd.DataFrame(columns=column_list)

	cluster = Cluster(cfg['cb_server'])
	authenticator = PasswordAuthenticator(cfg['cb_username'], cfg['cb_password'])
	cluster.authenticate(authenticator)
	cb_bucket_name = 'facerec_'+cfg['training_model_type']	
	cb_bucket = cluster.open_bucket(cb_bucket_name)
	cb_bucket.timeout = cfg['cb_bucket_timeout']
	cb_bucket.n1ql_timeout = 18000
	
	if cfg['training_model_type'] == 'fractal':
		n1ql_query = N1QLQuery("SELECT label, image_encodings FROM facerec_fractal where active = 'Y'")
	elif cfg['training_model_type'] == 'other':
		n1ql_query = N1QLQuery("SELECT label, image_encodings FROM facerec_other")
	else:
		n1ql_query = N1QLQuery("SELECT label, image_encodings FROM facerec_all where active = 'Y'")		
	n1ql_query.timeout = 18000
	row_iter = cb_bucket.n1ql_query(n1ql_query)

	print('\nCreating dataframe from CB output', type(row_iter))
	list_of_list = [row['image_encodings'].append(row['label']) for row in row_iter]
	columns = []
	df = pd.DataFrame(list_of_list, columns=column_list)
	'''
	i = 0
	for row in row_iter: 
		face_descriptor = row['image_encodings']
		face_descriptor.append(row['label'])
		df.loc[i] = face_descriptor
		i += 1
	'''
	print("len(df): ", len(df), len(list_of_list))
	new_model_path = cfg['trained_model_dir']+'/'+'model_'+str(len(df.label.unique().tolist()))+'_'+datetime.now().date().isoformat()+'.pkl'
	train_model_fromCB(df, new_model_path)	

def train_ml_model(df, model_file):	
	y = df['label']
	X = df.drop(['label','image_path','active', 'insert_time'],  axis=1)
	##X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	##print("classes",len([name for name in set(y)]))
	svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	svm.fit(X, y)
	with open(model_file, 'wb') as fp:
		pickle.dump(svm, fp)

def read_encodings():
	con_string='mysql+pymysql://'+cfg['sqlusr']+':'+cfg['sqlpwd']+'@localhost/'+cfg['sqldb']
	con=create_engine(con_string)
	try:
		if cfg['training_model_type'] == 'fractal':
			print('Training for Fractal Employees')
			query=("SELECT * from encoding_table where active='Y'")
			embeddings=pd.read_sql(query, con=con)
		elif cfg['training_model_type'] == 'other':
			print('Training for famous personalities')
			query=("SELECT * from encoding_table_other")
			embeddings=pd.read_sql(query, con=con)
		else:		
			print('Training for all data we have for platform')
			query=("SELECT * from encoding_table_all")
			embeddings=pd.read_sql(query, con=con)

	except Exception as e:
		print(e)
	
	return embeddings


def train():
	try:
		# embeddings=read_encodings()
		embeddings = readEmbeddingsFromCB()
		#new_model_path = cfg['trained_model_dir']+'/'+'model_'+str(len(embeddings.label.unique().tolist()))+'_'+datetime.now().date().isoformat()+'.pkl'
		#train_ml_model(embeddings, new_model_path)
		return True
	except Exception as e:
		print(e)
		return False

if __name__=="__main__":
	print(train())
