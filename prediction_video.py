import dlib
import cv2
import pickle
import numpy as np
import os
import sys

from skimage.transform import resize
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import transforms as transforms
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import multiprocessing
from multiprocessing import Process
import yaml
import time

from face_recognizer_feedforward_pytorch import FeedforwardNeuralNetModel
import json
import logging

# initializing the log settings
try:
	logging.basicConfig(filename = 'facereg_mumbai.log', level = logging.INFO)
except Exception as e:
    print("Error in initializing logging")
    sys.exit()

cut_size = 44

cfg_vgg = {
    'VGG19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M']
}

class VGG(nn.Module):
    def __init__(self, vgg_name):
        super(VGG, self).__init__()
        self.features = self._make_layers(cfg_vgg[vgg_name])
        self.classifier = nn.Linear(512, 7)

    def forward(self, x):
        out = self.features(x)
        out = out.view(out.size(0), -1)
        out = F.dropout(out, p=0.5, training=self.training)
        out = self.classifier(out)
        return out

    def _make_layers(self, cfg):
        layers = []
        in_channels = 3
        for x in cfg:
            if x == 'M':
                layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
            else:
                layers += [nn.Conv2d(in_channels, x, kernel_size=3, padding=1),
                           nn.BatchNorm2d(x),
                           nn.ReLU(inplace=True)]
                in_channels = x
        layers += [nn.AvgPool2d(kernel_size=1, stride=1)]
        return nn.Sequential(*layers)

def preprocess_input(x, v2=True):
	x = x.astype('float32')
	x = x / 255.0
	if v2:
		x = x - 0.5
		x = x * 2.0
	return x

def data(predicted_id, predicted_name, pred_probablity,  gender, gender_confidence, emotion, mail, designation, doj):
	face={}
	#face['faceId'] = k
	face['empId'] = predicted_id
	face['name'] = predicted_name
	face['confidence'] = pred_probablity
	face['gender'] = gender
	face['gender_confidence'] = gender_confidence
	face['emotion'] = emotion
	face['mailid'] = mail
	face['designation'] = designation
	face['doj'] = doj
	return face

# def face_prediction(procnum, clf, face_encodings, return_dict, PROB_THRESH = .5):
def face_prediction_svc(clf, face_encodings, PROB_THRESH = .5):	
	#probas = clf.predict_proba(np.array(face_encodings))
	probas = clf.predict_proba(face_encodings)
	max_probas = np.max(probas, axis=1) 
	
	is_recognized = [max_probas[i] > PROB_THRESH for i in range(max_probas.shape[0])]
	return [(pred, prob) if rec else ("Other", prob) for pred, prob, rec in zip(clf.predict(face_encodings), max_probas, is_recognized)]

def face_prediction_pytorch(model, face_encoding, PROB_THRESH = .5):

	face_encoding = torch.FloatTensor(face_encoding)
	if torch.cuda.is_available():		
		face_encoding = Variable(face_encoding.cuda())
	else:
		face_encoding = Variable(face_encoding)

	face_encoding = face_encoding.float().unsqueeze(0)

	# pass it through the model
	prediction = model(face_encoding)
	h_x = torch.mean(F.softmax(prediction, 1), dim=0).data
	probs, idx = h_x.sort(0, True)

	probs_arr = probs.cpu().numpy()
	idx_arr = idx.cpu().numpy()
	
	#print(probs_arr[0], idx_arr[0])
	return (probs_arr[0], idx_arr[0])

def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def rgb2gray(rgb):
		    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])


try:
	with open("./config.yml", 'r') as ymlfile:
			cfg = yaml.load(ymlfile)
	upsample_factor= 1
	PROB_THRESH= cfg['probablity_threshold']
	selectedModelNum = 0
except Exception as e:
	logging.exception(str(e))
	print("Error in loading config")
	sys.exit()

# Initialize hog face detector
try: 
	detector = dlib.get_frontal_face_detector()
except Exception as e:
	logging.exception(str(e))
	print("Error in Initializing hog face detector")
	sys.exit()


# Initialize cnn face detector
try:
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cfg['cnn_face_detector_path'])
except Exception as e:
	logging.exception(str(e))
	print("Error in Initializing cnn face detector")
	sys.exit()

# Initialize cnn face rec
try:
	facerec = dlib.face_recognition_model_v1(cfg['face_rec_model_path'])
except Exception as e:
	logging.exception(str(e))
	print("Error in Initializing cnn face recognition")
	sys.exit()

try:
	shape_predictor = dlib.shape_predictor(cfg['predictor_path'])
except Exception as e:
	logging.exception(str(e))
	print("Error in shape predictor")
	sys.exit()

try:
	if cfg['model_type'] == 'pytorch':
		# Store lableEncoder
		with open(cfg['label_encoder_path'], "rb") as handle:
			le = pickle.load(handle)
		print("Loaded Face Labels")

		input_dim = 128
		hidden_dim = 200
		output_dim = len(list(le.classes_))

		# Load face SVM based face predictor
		model = FeedforwardNeuralNetModel(input_dim, hidden_dim, output_dim)
		if torch.cuda.is_available():
			model.cuda()
		print("Initialized Pytorch Model")

		# model_load_path='fractal_469_model_2019-02-19.pth'	
		model.load_state_dict(torch.load(cfg['pytorch_model_path']))
		print("Loaded Pytorch Model")
	elif cfg['model_type'] == 'svc':

		if cfg['facerec_model_type'] == 0:
			model_path = cfg['model_save_path']		
		else:
			model_path = cfg['others_model_path']

		#Load face recognition model
		with open(model_path, 'rb') as f:
			u = pickle._Unpickler(f)
			u.encoding = 'latin1'
			clf = u.load()
	else:
		print("Please select correct model type")

except Exception as e:
	logging.exception(str(e))
	print("Error in loading pytorch or svc model")
	sys.exit()

#Load Gender Prediction model
try:
	gender_detect_path=cfg['gender_detection_path']
	with open(gender_detect_path, 'rb') as f:
		clf_gender = pickle.load(f, encoding='latin1')	
except Exception as e:
	logging.exception(str(e))
	print("Error in loading gender model")
	sys.exit()

# Initialize emotion model
try:
	class_names = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
	net = VGG('VGG19')
	checkpoint = torch.load(os.path.join('/home/rajneesh/DLProjects/FaceAI/Emotion/EmotionPytorch/FER2013_VGG19', 'PrivateTest_model.t7'))
	net.load_state_dict(checkpoint['net'])
	net.cuda()
	net.eval()
except Exception as e:
	logging.exception(str(e))
	print("Error in loading emotion model")
	sys.exit()

def emotion_detection(gray_face):
	try:
		transform_test = transforms.Compose([
									    transforms.TenCrop(cut_size),
									    transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
									])
		gray = rgb2gray(gray_face)
		gray = resize(gray, (48,48), mode='symmetric').astype(np.uint8)
		img = gray[:, :, np.newaxis]
		img = np.concatenate((img, img, img), axis=2)
		img = Image.fromarray(img)
		inputs = transform_test(img)
		ncrops, c, h, w = np.shape(inputs)
		inputs = inputs.view(-1, c, h, w)
		inputs = inputs.cuda()
		inputs = Variable(inputs, volatile=True)
		outputs = net(inputs)
		outputs_avg = outputs.view(ncrops, -1).mean(0)  # avg over crops
		score = F.softmax(outputs_avg)
		_, predicted = torch.max(outputs_avg.data, 0)

		return str(class_names[int(predicted.cpu().numpy())])
	except Exception as e:
		logging.exception(str(e))
		return "no_emotion_found"

def process_face(pool_list):
	left, top, right, bottom, emotion = pool_list

	try:
		dd = dlib.rectangle(left, top, right, bottom)
		shape = shape_predictor(frame, dd)

		# Compute face encoding (128D vectors)
		face_descriptor = facerec.compute_face_descriptor(frame, shape, 5)
		face_encodings = []
		face_encodings.append(face_descriptor)
		
		#get predictions
		if cfg['model_type'] == 'pytorch':
			(pred_probablity, face_id) = face_prediction_pytorch(model, face_descriptor, PROB_THRESH)
			predicted_face = le.inverse_transform(face_id)
			pred = []
			pred.append(predicted_face)
			pred.append(pred_probablity)
		elif cfg['model_type'] == 'svc':
			pred = face_prediction_svc(clf, face_encodings, PROB_THRESH)[0]
	except Exception as e:
		logging.exception(str(e))
		print("Error in recognition of face")
		sys.exit()

	try:
		gender_preds = face_prediction_svc(clf_gender, face_encodings, PROB_THRESH)
	except Exception as e:
		logging.exception(str(e))
		gender_preds = []
		gender_preds.append(["no_gender_found",0.0])

	custom_id = 0
	predictions = pred[0].split('_')
	
	if predictions[0]!='Other':
		predicted_id = predictions[0]
		predicted_name=' '.join(predictions[1:])
		pred_probablity = pred[1]

		# Custom IDs for External peoples
		if selectedModelNum == 1:
			custom_id += 1                  
			predicted_id =  'T0000'+str(custom_id)
			predicted_name=' '.join(predictions[0:])

	else:
		predicted_id = 'F00000'
		predicted_name='Other'
		pred_probablity = pred[1]

	#Load employee_details from the json file
	try:
		with open('employee_details.json') as f:
		    employee_data = json.load(f)

		if predicted_id in employee_data:
			mail = employee_data[predicted_id]["employee_details"]["email"]
			designation = employee_data[predicted_id]["employee_details"]["designation"]
			doj = employee_data[predicted_id]["employee_details"]["doj"]
		else:
			mail = "no_mail_found"
			designation = "no_designation_found"
			doj = "no_doj_found"
	except:
		logging.exception(str(e))
		mail = "no_mail_found"
		designation = "no_designation_found"
		doj = "no_doj_found"

	return [predicted_id,predicted_name,pred_probablity,gender_preds, pool_list[4], mail, designation, doj]




try:
	# Initialize face detection mode
	face_detection_mode = cfg['face_detection_mode']

	video_capture = cv2.VideoCapture(0)
except Exception as e:
	logging.exception(str(e))
	print("Error in video streaming")
	sys.exit()

# image_size = (1080, 720)

while video_capture.isOpened():
	res = {}

	try:
		ret, frame = video_capture.read()
		rgb_image = np.array(frame)
	except Exception as e:
		logging.exception(str(e))
		continue

	t = int(round(time.time() * 1000))
	# height, width, _ = frame.shape
	# thickness = (width + height) // 300
	# font = ImageFont.truetype(font='/home/rajneesh/DLProjects/FaceAI/EC_Facerec/font/FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))
	# pil_image = Image.fromarray(frame)
	# draw = ImageDraw.Draw(pil_image)

	if ret:
		if face_detection_mode == 'cnn':
			detector = cnn_face_detector

		try:			
			dets = face_detection(detector, frame, upsample_factor)
		except Exception as e:
			logging.exception(str(e))
			continue

		#If no faces are detected ignore
		if(len(dets)!=0):
			pool_list = []
			for k, d in enumerate(dets):
				if face_detection_mode == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom =  d.left(), d.top(), d.right(), d.bottom()

				try:
					gray_face = rgb_image[top:bottom, left:right]
					if not gray_face.any():
						emotion = "no_emotion_found"
					else:
						emotion = emotion_detection(gray_face)
				except Exception as e:
					logging.exception(str(e))
					emotion = "no_emotion_found"

				pool_list.append([left, top, right, bottom, emotion])

			print("Number of faces detected: {}".format(len(pool_list)))

			if len(pool_list) == 0:
				continue

			try:
				if cfg['model_type'] == 'pytorch':
					pool_ret = []
					for item in pool_list:
						pool_ret.append(process_face(item))
				elif cfg['model_type'] == 'svc':
					num_processes = len(pool_list)
					pool = multiprocessing.Pool(processes = num_processes)
					pool_ret = pool.map(process_face, pool_list)
			except Exception as e:
				logging.exception(str(e))
				continue

			for k,item in enumerate(pool_ret):
				predicted_id = item[0]
				predicted_name = item[1]
				pred_probablity = item[2]
				gender_preds = item[3]
				emotion = item[4]
				mail = item[5]
				designation = item[6]
				doj = item[7]

				face=data(predicted_id,predicted_name,pred_probablity,gender_preds[0][0],gender_preds[0][1],emotion,mail, designation, doj)
				res[k] = face
				# left, top, right, bottom, emotion = item[4]
				# label = '{} {}\n {}\n {}\n {:.2f}\n {}'.format(predicted_id,predicted_name,pred_probablity,gender_preds[0][0], gender_preds[0][1], emotion)
				# label_size = draw.textsize(label, font)
				# label_w, label_h = label_size

				# for i in range(thickness):
				# 	draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
									
				# new_bottom = min(bottom+label_h+2, height-2)
				# label_origin_bottom = new_bottom-label_h-2

				# if label_w < (right - left):
				# 	label_w = (right - left)

				# draw.rectangle([left, label_origin_bottom, left+label_w, new_bottom], fill=(0, 0, 150))					
				# draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)

	t1 = int(round(time.time() * 1000))
	print("Total process time: {}".format(t1-t))
	print(res)

	# img_display = np.array(pil_image.resize(width/2, height/2))
	# img_display = cv2.resize(np.array(pil_image), image_size)
	# cv2.imshow('FaceRecognition', np.array(img_display))
	
	# if cv2.waitKey(1) & 0xFF == ord('q'):
	# 	break