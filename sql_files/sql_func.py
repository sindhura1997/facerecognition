#MY SQL commands
# create database facedb;
# create user 'shreya'@'localhost' identified by 'fractal@1234';
# grant all on facedb.* to 'shreya' identified by 'fractal@1234';
# exit
# mysql -u 
# use facedb;
# create table encodings (encode_1 INT NOT NULL,encode_2 INT NOT NULL,label TEXT);
# show columns from encodings;
# INSERT INTO encodings VALUES (12,13,'F02307_shreya_garg'),(14,25,'F01234_test_user');
# grant all privileges on facedb.* to 'shreya'@'localhost' identified by 'fractal@1234';
# mysql -u 'shreya' -p facedb -e 'select * from encoding_data;' > ./res.txt

#Install mysql connector python
# pip install mysql-connector-python
# pip install pymysql
# 
#code snippet to insert data in table encodings

import mysql.connector
import pandas as pd
cnx=mysql.connector.connect(user='shreya',password='fractal@1234',host='127.0.0.1',database='facedb')
cursor = cnx.cursor()
add_encoding=("INSERT INTO encodings VALUES(45,44,'F00000_python_test')")
cursor.execute(add_encoding)
cnx.commit()
cursor.close()
cnx.close()

# Read table and store as df
embeddings=pd.read_sql(query,con=cnx)

# Using sqlalchemy
from sqlalchemy import create_engine
con=create_engine('mysql+pymysql://shreya:fractal@1234@localhost/facedb');

# write table to df
with con.connect() as conn, conn.begin():
	embeddings=pd.read_sql(query,con=cnx)

# write df to table
with con.connect() as conn, conn.begin():
	embeddings.to_sql('encodings',conn, if_exists='append', index = False)
conn.close()
