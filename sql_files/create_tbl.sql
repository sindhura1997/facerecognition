#CREATE DATABASE facedb;
#USE facedb;

CREATE TABLE encoding_table (
encode_1 DECIMAL(30,25) NOT NULL,
encode_2 DECIMAL(30,25) NOT NULL,
encode_3 DECIMAL(30,25) NOT NULL,
encode_4 DECIMAL(30,25) NOT NULL,
encode_5 DECIMAL(30,25) NOT NULL,
encode_6 DECIMAL(30,25) NOT NULL,
encode_7 DECIMAL(30,25) NOT NULL,
encode_8 DECIMAL(30,25) NOT NULL,
encode_9 DECIMAL(30,25) NOT NULL,
encode_10 DECIMAL(30,25) NOT NULL,
encode_11 DECIMAL(30,25) NOT NULL,
encode_12 DECIMAL(30,25) NOT NULL,
encode_13 DECIMAL(30,25) NOT NULL,
encode_14 DECIMAL(30,25) NOT NULL,
encode_15 DECIMAL(30,25) NOT NULL,
encode_16 DECIMAL(30,25) NOT NULL,
encode_17 DECIMAL(30,25) NOT NULL,
encode_18 DECIMAL(30,25) NOT NULL,
encode_19 DECIMAL(30,25) NOT NULL,
encode_20 DECIMAL(30,25) NOT NULL,
encode_21 DECIMAL(30,25) NOT NULL,
encode_22 DECIMAL(30,25) NOT NULL,
encode_23 DECIMAL(30,25) NOT NULL,
encode_24 DECIMAL(30,25) NOT NULL,
encode_25 DECIMAL(30,25) NOT NULL,
encode_26 DECIMAL(30,25) NOT NULL,
encode_27 DECIMAL(30,25) NOT NULL,
encode_28 DECIMAL(30,25) NOT NULL,
encode_29 DECIMAL(30,25) NOT NULL,
encode_30 DECIMAL(30,25) NOT NULL,
encode_31 DECIMAL(30,25) NOT NULL,
encode_32 DECIMAL(30,25) NOT NULL,
encode_33 DECIMAL(30,25) NOT NULL,
encode_34 DECIMAL(30,25) NOT NULL,
encode_35 DECIMAL(30,25) NOT NULL,
encode_36 DECIMAL(30,25) NOT NULL,
encode_37 DECIMAL(30,25) NOT NULL,
encode_38 DECIMAL(30,25) NOT NULL,
encode_39 DECIMAL(30,25) NOT NULL,
encode_40 DECIMAL(30,25) NOT NULL,
encode_41 DECIMAL(30,25) NOT NULL,
encode_42 DECIMAL(30,25) NOT NULL,
encode_43 DECIMAL(30,25) NOT NULL,
encode_44 DECIMAL(30,25) NOT NULL,
encode_45 DECIMAL(30,25) NOT NULL,
encode_46 DECIMAL(30,25) NOT NULL,
encode_47 DECIMAL(30,25) NOT NULL,
encode_48 DECIMAL(30,25) NOT NULL,
encode_49 DECIMAL(30,25) NOT NULL,
encode_50 DECIMAL(30,25) NOT NULL,
encode_51 DECIMAL(30,25) NOT NULL,
encode_52 DECIMAL(30,25) NOT NULL,
encode_53 DECIMAL(30,25) NOT NULL,
encode_54 DECIMAL(30,25) NOT NULL,
encode_55 DECIMAL(30,25) NOT NULL,
encode_56 DECIMAL(30,25) NOT NULL,
encode_57 DECIMAL(30,25) NOT NULL,
encode_58 DECIMAL(30,25) NOT NULL,
encode_59 DECIMAL(30,25) NOT NULL,
encode_60 DECIMAL(30,25) NOT NULL,
encode_61 DECIMAL(30,25) NOT NULL,
encode_62 DECIMAL(30,25) NOT NULL,
encode_63 DECIMAL(30,25) NOT NULL,
encode_64 DECIMAL(30,25) NOT NULL,
encode_65 DECIMAL(30,25) NOT NULL,
encode_66 DECIMAL(30,25) NOT NULL,
encode_67 DECIMAL(30,25) NOT NULL,
encode_68 DECIMAL(30,25) NOT NULL,
encode_69 DECIMAL(30,25) NOT NULL,
encode_70 DECIMAL(30,25) NOT NULL,
encode_71 DECIMAL(30,25) NOT NULL,
encode_72 DECIMAL(30,25) NOT NULL,
encode_73 DECIMAL(30,25) NOT NULL,
encode_74 DECIMAL(30,25) NOT NULL,
encode_75 DECIMAL(30,25) NOT NULL,
encode_76 DECIMAL(30,25) NOT NULL,
encode_77 DECIMAL(30,25) NOT NULL,
encode_78 DECIMAL(30,25) NOT NULL,
encode_79 DECIMAL(30,25) NOT NULL,
encode_80 DECIMAL(30,25) NOT NULL,
encode_81 DECIMAL(30,25) NOT NULL,
encode_82 DECIMAL(30,25) NOT NULL,
encode_83 DECIMAL(30,25) NOT NULL,
encode_84 DECIMAL(30,25) NOT NULL,
encode_85 DECIMAL(30,25) NOT NULL,
encode_86 DECIMAL(30,25) NOT NULL,
encode_87 DECIMAL(30,25) NOT NULL,
encode_88 DECIMAL(30,25) NOT NULL,
encode_89 DECIMAL(30,25) NOT NULL,
encode_90 DECIMAL(30,25) NOT NULL,
encode_91 DECIMAL(30,25) NOT NULL,
encode_92 DECIMAL(30,25) NOT NULL,
encode_93 DECIMAL(30,25) NOT NULL,
encode_94 DECIMAL(30,25) NOT NULL,
encode_95 DECIMAL(30,25) NOT NULL,
encode_96 DECIMAL(30,25) NOT NULL,
encode_97 DECIMAL(30,25) NOT NULL,
encode_98 DECIMAL(30,25) NOT NULL,
encode_99 DECIMAL(30,25) NOT NULL,
encode_100 DECIMAL(30,25) NOT NULL,
encode_101 DECIMAL(30,25) NOT NULL,
encode_102 DECIMAL(30,25) NOT NULL,
encode_103 DECIMAL(30,25) NOT NULL,
encode_104 DECIMAL(30,25) NOT NULL,
encode_105 DECIMAL(30,25) NOT NULL,
encode_106 DECIMAL(30,25) NOT NULL,
encode_107 DECIMAL(30,25) NOT NULL,
encode_108 DECIMAL(30,25) NOT NULL,
encode_109 DECIMAL(30,25) NOT NULL,
encode_110 DECIMAL(30,25) NOT NULL,
encode_111 DECIMAL(30,25) NOT NULL,
encode_112 DECIMAL(30,25) NOT NULL,
encode_113 DECIMAL(30,25) NOT NULL,
encode_114 DECIMAL(30,25) NOT NULL,
encode_115 DECIMAL(30,25) NOT NULL,
encode_116 DECIMAL(30,25) NOT NULL,
encode_117 DECIMAL(30,25) NOT NULL,
encode_118 DECIMAL(30,25) NOT NULL,
encode_119 DECIMAL(30,25) NOT NULL,
encode_120 DECIMAL(30,25) NOT NULL,
encode_121 DECIMAL(30,25) NOT NULL,
encode_122 DECIMAL(30,25) NOT NULL,
encode_123 DECIMAL(30,25) NOT NULL,
encode_124 DECIMAL(30,25) NOT NULL,
encode_125 DECIMAL(30,25) NOT NULL,
encode_126 DECIMAL(30,25) NOT NULL,
encode_127 DECIMAL(30,25) NOT NULL,
encode_128 DECIMAL(30,25) NOT NULL,
label TEXT,
image_path TEXT,
active VARCHAR(1) NOT NULL DEFAULT 'Y',
insert_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP);

#id INT AUTO_INCREMENT UNIQUE KEY,
