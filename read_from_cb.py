from couchbase import Couchbase
from couchbase.cluster import Cluster
from couchbase.cluster import PasswordAuthenticator
from couchbase.bucket import Bucket
from couchbase.exceptions import CouchbaseError
from couchbase.exceptions import CouchbaseTransientError

from couchbase.n1ql import N1QLQuery
from apscheduler.schedulers.blocking import BlockingScheduler
import time
import json

def read_employee_details():
	cluster = Cluster("couchbase://172.16.28.124:8091/")
	authenticator = PasswordAuthenticator("administrator", "administrator")
	cluster.authenticate(authenticator)
	cb_bucket = cluster.open_bucket("employee_details")
	cb_bucket.timeout = 200
	cb_bucket.n1ql_timeout = 3600

	n1ql_query = N1QLQuery("SELECT * FROM employee_details")
	n1ql_query.timeout = 3600
	row_iter = cb_bucket.n1ql_query(n1ql_query)
	details_list = {}
	for row in row_iter:
		details_list[row["employee_details"]["id"]] = row	
	with open('employee_details.json', 'w') as outfile:
	    json.dump(details_list, outfile)

scheduler = BlockingScheduler()
scheduler.add_job(read_employee_details, 'interval', hours=1)
scheduler.start()
