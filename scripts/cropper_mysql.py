#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import numpy as np
import random
import time
import datetime
from PIL import Image
import pandas as pd
import errno
import csv
import pandas as pd
from sqlalchemy import create_engine
import yaml

with open("./config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

def add_new_encoding(new_df):
	con_string='mysql+pymysql://'+cfg['sqlusr']+':'+cfg['sqlpwd']+'@localhost/'+cfg['sqldb']
	con=create_engine(con_string)
	#To make column names same as mysql table headers
	#Run below if column names are different for new_df and mysql table
	q=("select column_name from information_schema.columns where table_name='encoding_table'")
	cols=pd.read_sql(q,con=con)
	new_cols = {x: y for x, y in zip(new_df.columns, cols['column_name'].tolist())}
	new_df=new_df.rename(columns=new_cols)
	indx=0
	total=len(new_df)
	#Add new_df data to mysql table
	while(indx<total):
		if indx+5000<total:
			lmt=indx+5000
		else:
			lmt=total
		with con.connect() as conn, conn.begin():
			print(indx,lmt)
			#new_df.to_sql('encoding_table', conn, if_exists='append', index = False)
			new_df.iloc[indx:lmt].to_sql('encoding_table', conn, if_exists='append', index = False)
		indx=lmt

def face_encoder():
	shape_predictor = dlib.shape_predictor(cfg['predictor_path'])
	facerec_model = dlib.face_recognition_model_v1(cfg['face_rec_model_path'])
	know_face_encoding = []
	known_faces = []
	#column_list = list(range(128))
	column_list = ['encode_'+str(i) for i in range(128)]
	column_list.append('label')
	column_list.append('image_path')
	#column_list.append('insert_time')

	df = pd.DataFrame(columns=column_list)
	i = 0
	
	for dir in os.listdir(cfg['CROP_DIR']):
		all_images = os.listdir(cfg['CROP_DIR']+'/'+dir)
		for each_image in all_images:
			try:
				image_path  = cfg['CROP_DIR']+'/'+dir+'/'+each_image
				image = cv2.imread(image_path)		
				height, width, _ = image.shape
				dd = dlib.rectangle(0, 0, width, height)
				shape = shape_predictor(image, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = list(facerec_model.compute_face_descriptor(image, shape))
				face_descriptor.append(str(dir))
				face_descriptor.append(str(each_image))	
				
				# ============ Added by Rajneesh to insert datetime =============
				#ts = time.time()
				#timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')	
				#face_descriptor.append(str(timestamp))		
				# ============ Added by Rajneesh to insert datetime =============	
				df.loc[i] = face_descriptor
				#print(df.loc[i])
				i += 1
			except:
				continue
	return df

def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def crop():
	
	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	cnn_face_detector = dlib.cnn_face_detection_model_v1(cfg['cnn_face_detector_path'])

	DETECTION_MODEL = 'cnn' # or 'cnn'
	UPSAMPLE_FACTOR = 1
	margin = 10

	label_count = 0
	try:
		os.makedirs(cfg['CROP_DIR'])
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass
				
	for child_dir in os.listdir(cfg['FRAMES_AUG_DIR']):
		label = child_dir
		print ("Processing: ", label)
		label_count += 1

		for image_name in os.listdir(cfg['FRAMES_AUG_DIR']+'/'+child_dir):
			image_path = cfg['FRAMES_AUG_DIR']+'/'+child_dir+'/'+image_name
			
			# print(image_path)
			try:
				image = cv2.imread(image_path)
				if DETECTION_MODEL == 'cnn':
					detector = cnn_face_detector	

				dets = face_detection(detector, image, UPSAMPLE_FACTOR)
				# Now process each face we found.
				face_encodings = []
				for k, d in enumerate(dets):
					
					if DETECTION_MODEL == 'cnn':
						left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
					else:
						left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
					
					#save_image_name = int(round(time.time() * 1000))
					cropped_img = image[top-margin:bottom+margin, left-margin:right+margin]
					height, width = cropped_img.shape[:2]
					top = max(0, top+2)
					left = max(0, left+2)
					right = min(width, right+2)
					bottom = min(bottom, right+2)
					
					save_dir = cfg['CROP_DIR']+'/'+label
					# print(save_dir)
					if not os.path.exists(save_dir):
						os.mkdir(save_dir)

					# print(save_dir+'/'+image_name, cropped_img)
					cv2.imwrite(save_dir+'/'+image_name, cropped_img)
			except Exception as e:
				print (image_path, e)
				continue

def main():
	crop()
	new_encodings=face_encoder()
	new_encodings.to_csv('/opt/FaceRecognition/pipeline/test.csv', sep=',', encoding='utf-8')
	add_new_encoding(new_encodings)

if __name__ == '__main__':
	#main(sys.argv[1],sys.argv[2],sys.argv[3])
	main()	
