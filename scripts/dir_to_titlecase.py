import os
import sys
import shutil

dir_path = './cropped_title/'
tar_path = './cropped/'

all_dirs = os.listdir(dir_path)

for each_dir in all_dirs:
	titlecase_name = str.title(each_dir)

	try:
		shutil.move(dir_path+each_dir, tar_path+titlecase_name)
	except Exception as e:
		print(each_dir, e)
		continue