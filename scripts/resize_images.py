import os
import sys
from PIL import Image

img_dir_path = '/opt/FaceRecognition/EC_FaceRecNew/static/images_n/'
all_images = os.listdir(img_dir_path)

for each_image in all_images:
	img_path = img_dir_path+each_image
	img = Image.open(img_path)
	img = img.resize((400, 400), Image.ANTIALIAS)
	img.save(img_path)
	
