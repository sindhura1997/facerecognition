import os
import pandas as pd
import csv

dir_path = '/data/Facerecognition/facerec_dev/pipeline/cropped/'
#write_csv = 'print_csv.csv'
active_emp_csv = 'active_emp.csv'

csv_lines = []
header_list = ['Employee_id','Employee_name']
csv_lines.append(header_list)

all_existing_emps = os.listdir(dir_path)
all_existing_emp = [each_dir.split('_')[0].strip().lower() for each_dir in all_existing_emps]
#print(all_existing_emp)

emp_df = pd.read_csv(active_emp_csv)
for index, row in emp_df.iterrows():
	emp_id = row['Empid']
	name = row['Name']

	try:
		if not emp_id.lower() in all_existing_emp:
			csv_line = [emp_id,name]
			csv_lines.append(csv_line)
			print(emp_id.lower())
	except Exception as e:
		print(row, e)

with open(write_csv, 'w') as writeFile:
    writer = csv.writer(writeFile)
    writer.writerows(csv_lines)

writeFile.close()