import os
import sys
import shutil
from PIL import Image

display_images_path = '/opt/FaceRecognition/EC_FaceRecNew/static/images/'
facerec_images_path = '/opt/FaceRecognition/pipeline/Archive/frames_archive/'
new_display_images = '/opt/FaceRecognition/EC_FaceRecNew/static/images_n/'

all_display_images = os.listdir(display_images_path)
all_display_images = [each_display_image.split('.')[0] for each_display_image in all_display_images]
all_facerec_images = os.listdir(facerec_images_path)
#print(all_facerec_images)

for each_facerec_image in all_facerec_images:
	if each_facerec_image.split('_')[0] not in all_display_images:
		#print(each_facerec_image)
		source_file_name = facerec_images_path+each_facerec_image+'/8.jpg'
		target_file_name = new_display_images+each_facerec_image.split('_')[0]+'.jpg'
		#print(source_file_name)
		if not os.path.exists(source_file_name):
			print(source_file_name, target_file_name)
			continue;
		shutil.copy(source_file_name, target_file_name)
