#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import random
import time
from PIL import Image

import numpy as np
import pandas as pd
import pickle

import torch
import torch.nn as nn
import torchvision.transforms as transforms
import torchvision.datasets as dsets
from torch.autograd import Variable

from sklearn.model_selection import train_test_split
from sklearn import preprocessing

'''
STEP 3: CREATE MODEL CLASS
'''
class FeedforwardNeuralNetModel(nn.Module):
	def __init__(self, input_dim, hidden_dim, output_dim):
		super(FeedforwardNeuralNetModel, self).__init__()
		# Linear function 1: 128 --> 200
		self.fc1 = nn.Linear(input_dim, hidden_dim) 
		# Non-linearity 1
		self.relu1 = nn.ReLU()
		
		# Linear function 2: 200 --> 200
		self.fc2 = nn.Linear(hidden_dim, hidden_dim)
		# Non-linearity 2
		self.relu2 = nn.ReLU()			
		
		'''
		# Linear function 3: 200 --> 200
		self.fc3 = nn.Linear(hidden_dim, hidden_dim)
		# Non-linearity 3
		self.relu3 = nn.ReLU()

		# Linear function 4: 200 --> 200
		self.fc4 = nn.Linear(hidden_dim, hidden_dim)
		# Non-linearity 4
		self.relu4 = nn.ReLU()
		'''
		
		# Linear function 5 (readout): 200 --> 150
		self.fc5 = nn.Linear(hidden_dim, output_dim)  
	
	def forward(self, x):
		# Linear function 1
		out = self.fc1(x)
		# Non-linearity 1
		out = self.relu1(out)
		
		
		# Linear function 2
		out = self.fc2(out)
		# Non-linearity 2
		out = self.relu2(out)

		'''
		# Linear function 3
		out = self.fc3(out)
		# Non-linearity 3
		out = self.relu3(out)

		
		# Linear function 4
		out = self.fc4(out)
		# Non-linearity 4
		out = self.relu4(out)
		'''

		
		# Linear function 5 (readout)
		out = self.fc5(out)
		return out

def train_ml_model(df, model_save_path):

	input_dim = 128
	hidden_dim = 200
	output_dim = 150
	batch_size = 200

	y_1 = df['label']
	y_1.to_csv('fractal_face_lables.csv')
	X = df.drop('label',  axis=1)
	le = preprocessing.LabelEncoder()

	y = le.fit_transform(y_1)

	# Store lableEncoder
	with open("../models/lable_encoder.pkl", "wb") as handle:
		pickle.dump(le, handle, protocol=pickle.HIGHEST_PROTOCOL)

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=101)
	print X_train.shape, X_test.shape, y_train.shape, y_test.shape

	X_train = np.float64(X_train.as_matrix())
	X_test = np.float64(X_test.as_matrix())

	y_train = np.float64(y_train)
	y_test = np.float64(y_test)

	#print type(X_train), type(X_test), X_train.dtype, type(torch.from_numpy(X_train)), type(torch.from_numpy(X_test))
	train_dataset = torch.utils.data.TensorDataset(torch.from_numpy(X_train), torch.from_numpy(y_train))
	test_dataset = torch.utils.data.TensorDataset(torch.from_numpy(X_test), torch.from_numpy(y_test))

	train_loader = torch.utils.data.DataLoader(dataset=train_dataset, 
											batch_size=batch_size, 
											shuffle=True)

	test_loader = torch.utils.data.DataLoader(dataset=test_dataset, 
											batch_size=batch_size, 
											shuffle=False)
	
	'''
	STEP 4: INSTANTIATE MODEL CLASS
	'''

	model = FeedforwardNeuralNetModel(input_dim, hidden_dim, output_dim)

	#######################
	#  USE GPU FOR MODEL  #
	#######################

	if torch.cuda.is_available():
		model.cuda()

	'''
	STEP 5: INSTANTIATE LOSS CLASS
	'''
	criterion = nn.CrossEntropyLoss()


	'''
	STEP 6: INSTANTIATE OPTIMIZER CLASS
	'''
	learning_rate = 0.1
	optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

	'''
	STEP 7: TRAIN THE MODEL
	'''
	num_epochs = 100
	for epoch in range(num_epochs):
		for i, (images, labels) in enumerate(train_loader):
			
			#######################
			#  USE GPU FOR MODEL  #
			#######################
			if torch.cuda.is_available():
				images = Variable(images.float().cuda())
				labels = Variable(labels.long().cuda())
			else:
				images = Variable(images.float())
				labels = Variable(labels.long())
			
			# Clear gradients w.r.t. parameters
			optimizer.zero_grad()
			
			# Forward pass to get output/logits
			outputs = model(images)
			
			# Calculate Loss: softmax --> cross entropy loss
			loss = criterion(outputs, labels)
			
			# Getting gradients w.r.t. parameters
			loss.backward()
			
			# Updating parameters
			optimizer.step()
			
			#iter += 1

		# Calculate Accuracy on test data        
		correct = 0
		total = 0
		# Iterate through test dataset
		for images, labels in test_loader:
			#######################
			#  USE GPU FOR MODEL  #
			#######################
			if torch.cuda.is_available():
				images = Variable(images.float().cuda())
			else:
				images = Variable(images.float())
			
			# Forward pass only to get logits/output
			outputs = model(images)
			
			# Get predictions from the maximum value
			_, predicted = torch.max(outputs.data, 1)
			
			# Total number of labels
			total += labels.long().size(0)
			
			#######################
			#  USE GPU FOR MODEL  #
			#######################
			# Total correct predictions
			correct += (predicted.cpu() == labels.long().cpu()).sum()
		
		accuracy = 100 * correct / float(total)
		
		# Print Loss
		print('Epoch: {}. Loss: {}. Correct/Total: {}/{}, Accuracy: {:0.4f}'.format(epoch+1, loss.data[0], correct, total, accuracy))

	# Save pytorch model
	torch.save(model.state_dict(), model_save_path)

def _main():
	
	# Initialize dlib shape predictor
	predictor_path = '../models/shape_predictor_68_face_landmarks.dat'
	shape_predictor = dlib.shape_predictor(predictor_path)

	# Initialize cnn face detector
	face_rec_model_path = '../models/dlib_face_recognition_resnet_model_v1.dat'
	facerec = dlib.face_recognition_model_v1(face_rec_model_path)

	# Read images from image_directory
	#parent_dir_path = '/home/rajneesh/DLProjects/Facerecognition/images/face_rec_data/Mumbai_7th_Floor/cropped_faces/'
	parent_dir_path = '/home/rajneesh/DLProjects/Facerecognition/images/face_rec_data/cropped_all/'

	# Save model to a file
	model_save_path = '../models/pytorch_model_150_test.pth'
	encodings_file = 'face_encodings_7th_151.csv'
	input_dim = 128

	if not os.path.exists(encodings_file):

		X = []
		y = []
		data_dict = {}
		
		#create empty dataframe
		column_list = list(range(input_dim))
		column_list.append('label')
		df = pd.DataFrame(columns=column_list)
		i = 0
		for child_dir in os.listdir(parent_dir_path):
			label = child_dir
			print "Processing: ", label

			for image_name in os.listdir(parent_dir_path+child_dir):
				image_path = parent_dir_path+child_dir+'/'+image_name
				

				try:
					cropped_image = cv2.imread(image_path)
					width, height, _ = cropped_image.shape
					
					dd = dlib.rectangle(0, 0, width, height)
					shape = shape_predictor(cropped_image, dd)

					# Compute face encoding (128D vectors)
					face_descriptor = list(facerec.compute_face_descriptor(cropped_image, shape))
					face_descriptor.append(label)
					df.loc[i] = face_descriptor

					i += 1

				except:
					print image_path
					#os.remove(image_path)
					continue	

		df.to_csv(encodings_file, encoding='utf-8', index=False)
	else:
		#Read esisting embeding file
		df = pd.read_csv(encodings_file)		
		#print df.head()

	# train ML model
	train_ml_model(df, model_save_path)

if __name__ == '__main__':
	_main()

