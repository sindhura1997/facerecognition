#!/usr/bin/python

import sys
import os
import dlib
import glob
from skimage import io
import pickle
import cv2
import numpy as np
import colorsys
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import random
import yaml
import redis

def face_prediction(clf, face_encodings, face_locations, PROB_THRESH = .5):
	
	probas = clf.predict_proba(np.array(face_encodings))
	max_probas = np.max(probas, axis=1) 
	
	is_recognized = [max_probas[i] > PROB_THRESH for i in range(max_probas.shape[0])]

	# predict classes and cull classifications that are not with high confidence
	return [(pred, loc, prob) if rec else ("Other", loc, prob) for pred, loc, prob, rec in zip(clf.predict(face_encodings), face_locations, max_probas, is_recognized)]


def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def _main(detection_model_path, detection_mode, prediction_model_path, recognition_model_path, ml_model_savepath, camera_or_video):
	
	redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)
	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	dl_face_detector = dlib.cnn_face_detection_model_v1(detection_model_path)

	# Initialize dlib shape predictor
	shape_predictor = dlib.shape_predictor(prediction_model_path)

	# Initialize cnn face detector
	facerec_model = dlib.face_recognition_model_v1(recognition_model_path)

	# Load face SVM based face predictor
	with open(ml_model_savepath, 'rb') as f:
		clf = pickle.load(f)
	
	UPSAMPLE_FACTOR = 1
	PROB_THRESH = 0.80

	#video_capture = cv2.VideoCapture('http://172.16.6.116:8080/video?dummy=param.mjpg')
	video_capture = cv2.VideoCapture(camera_or_video)
	process_this_frame = True
	
	while(True):

		ret, frame = video_capture.read()
		height, width, _ = frame.shape

		font = ImageFont.truetype(font='FiraMono-Medium.otf', size=np.floor(5e-2 * height + 0.5).astype('int32'))
		thickness = (width + height) // 300

		if process_this_frame:
		
			if detection_mode == 'cnn':
				detector = dl_face_detector			
			dets = face_detection(detector, frame, UPSAMPLE_FACTOR)

			# Now process each face we found.
			face_encodings = []
			face_locations = []
			for k, d in enumerate(dets):
				
				if detection_mode == 'cnn':
					left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
				else:
					left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
				
				# Get the landmarks/parts for the face in box d.
				dd = dlib.rectangle(left, top, right, bottom)
				shape = shape_predictor(frame, dd)

				# Compute face encoding (128D vectors)
				face_descriptor = facerec_model.compute_face_descriptor(frame, shape, 10)

				face_encodings.append(face_descriptor)
				face_locations.append((left, top, right, bottom))

			pil_image = Image.fromarray(frame)
			draw = ImageDraw.Draw(pil_image)
			if len(face_encodings) > 0:
				preds = face_prediction(clf, face_encodings, face_locations, PROB_THRESH)

				# Plot face recognition
				for pred in preds:
					if pred[0] == 'Other':
						continue

					predictions = pred[0].split('_')
					if redis_db.get("dis"+predictions[0]):
						#print(redis_db.get(predictions[0]))
						pass
					else:
						if redis_db.get("evnt_"+predictions[0]):
							redis_db.set("dis"+predictions[0], predictions[1]+'_'+predictions[2]+' '+str(redis_db.get("evnt_"+predictions[0])), ex=5)
						else:
							redis_db.set("dis"+predictions[0], predictions[1]+'_'+predictions[2], ex=5)					

					predicted_face = pred[0]
					face_locations = pred[1]
					pred_probablity = pred[2]
					
					left, top, right, bottom = face_locations[0], face_locations[1], face_locations[2], face_locations[3]

					label = '{} {:.2f}'.format(predicted_face, pred_probablity)
					#label = predicted_face
					label_size = draw.textsize(label, font)
					label_w, label_h = label_size

					for i in range(thickness):
						draw.rectangle([left + i, top + i, right - i, bottom - i], outline=(255, 255, 255))
										
					new_bottom = min(bottom+label_h+2, height-2)
					label_origin_bottom = new_bottom-label_h-2

					if label_w < (right - left):
						label_w = (right - left)

					draw.rectangle([left, label_origin_bottom, left+label_w, new_bottom], fill=(0, 0, 150))					
					draw.text(np.array([left, bottom - 2]), label, fill=(255, 255, 255), font=font)
				

		img_display = cv2.resize(np.array(pil_image), (width, height))
		cv2.imshow('FaceRecognition', np.array(img_display))

		process_this_frame = not process_this_frame
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

if __name__ == '__main__':	

	with open("config.yml", 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

	_main(cfg['dl_face_detection_path'], cfg['detection_mode'], cfg['dl_shape_predictor_path'], cfg['dl_face_recognition_path'], cfg['ml_model_path'], cfg['camera_or_video'])