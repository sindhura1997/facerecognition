'''
# This script is used to read captured images and crops faces into croped directory
'''

import sys
import os
import dlib
import pickle
import cv2
import numpy as np
import random
import time
from PIL import Image
import errno

def face_detection(detector, image, upsample_factor):
	return detector(image, upsample_factor)

def crop(frames_dir, crops_dir, detection_model_path, detection_mode):

	# Initialize hog face detector
	detector = dlib.get_frontal_face_detector()

	# Initialize cnn face detector
	dl_face_detector = dlib.cnn_face_detection_model_v1(detection_model_path)

	UPSAMPLE_FACTOR = 1
	margin = 10
	label_count = 0

	dirs=os.listdir(frames_dir)
	for dir in dirs:
		try:
			if not os.path.exists(crops_dir+dir):
				os.makedirs(crops_dir+dir)
		except OSError as e:
			if e.errno != errno.EEXIST:
				print('Cropper Line 42: ',e)
				return False
			else:
				pass
		
		print('Processing: ', dir)			
		for image_name in os.listdir(frames_dir+dir):
			image_path = frames_dir+dir+'/'+image_name
			try:
				image = cv2.imread(image_path)
				if detection_mode == 'cnn':
					detector = dl_face_detector	

				dets = face_detection(detector, image, UPSAMPLE_FACTOR)
				
				# Now process each face we found.
				face_encodings = []
				for k, d in enumerate(dets):
						
					if detection_mode == 'cnn':
						left, top, right, bottom = d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()
					else:
						left, top, right, bottom = 	d.left(), d.top(), d.right(), d.bottom()
						
					cropped_img = image[top-margin:bottom+margin, left-margin:right+margin]
					height, width = cropped_img.shape[:2]
					top = max(0, top+2)
					left = max(0, left+2)
					right = min(width, right+2)
					bottom = min(bottom, right+2)

					cv2.imwrite(crops_dir+dir+'/'+dir.split('_')[0]+image_name, cropped_img)
				
			except Exception as e:
				print('Cropper Line 74: ',e)
				continue

	return True

'''
if __name__ == '__main__':
	crop()
'''