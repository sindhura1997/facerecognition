#!/usr/bin/python

import sys
import os
import dlib
import pickle
import cv2
import random
import time
from PIL import Image

import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

def train_ml_model(df, model_file):

	print("Training Starts........")	
	y = df['label']
	X = df.drop('label',  axis=1)

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	
	st_time = time.time()
	print("\nFitting the classifier to the training set")
	
	svm = SVC(kernel='linear', C=10, gamma=0.001, probability=True)
	svm.fit(X_train, y_train)		

	ed_time = time.time()
	print('Time in classification {0: .2f} seconds'.format((ed_time-st_time)/1000))
	print("\nPredicting people's names on the test set")
	y_pred = svm.predict(X_test)

	# Create a list of class names
	class_names = [name for name in set(y)]
	print(classification_report(y_test, y_pred, target_names=class_names))

	# Now train on full dataset
	print("\nTraining on full dataset")
	svm.fit(X, y)
	
	with open(model_file, 'wb') as fp:
		pickle.dump(svm, fp)

	print("Training Completed........")

def trainer(prediction_model_path, recognition_model_path, crops_dir, ml_model_savepath, encoding_path):
		
	print("Encoding Starts........")
	# Initialize dlib shape predictor
	shape_predictor = dlib.shape_predictor(prediction_model_path)

	# Initialize cnn face detector
	#dl_face_detector = dlib.face_recognition_model_v1(dl_model_path)
	#dl_face_detector = dlib.cnn_face_detection_model_v1('/opt/FaceRecognition/CodeforUnion/models/dlib_face_recognition_resnet_model_v1.dat')
	dl_model_path = './models/dlib_face_recognition_resnet_model_v1.dat'
	facerec_model = dlib.face_recognition_model_v1(recognition_model_path)
	input_dim = 128

	if not os.path.exists(encoding_path):

		X = []
		y = []

		# create empty dataframe
		column_list = list(range(input_dim))
		column_list.append('label')
		df = pd.DataFrame(columns=column_list)
		i = 0
		
		for child_dir in os.listdir(crops_dir):
			label = child_dir
			print("Processing: ", label)

			for image_name in os.listdir(crops_dir+child_dir):
				image_path = crops_dir+child_dir+'/'+image_name
				
				try:
					cropped_image = cv2.imread(image_path)
					width, height, _ = cropped_image.shape
					
					dd = dlib.rectangle(0, 0, width, height)
					shape = shape_predictor(cropped_image, dd)

					# Compute face encoding (128D vectors)
					face_descriptor = list(facerec_model.compute_face_descriptor(cropped_image, shape))
					face_descriptor.append(label)
					df.loc[i] = face_descriptor
					i += 1

				except Exception as e:
					print(image_path, e)
					os.remove(image_path)
					continue	

		print("Encoding Completed")
		df.to_csv(encoding_path, encoding='utf-8', index=False)
	else:
		#Read existing embeding file
		df = pd.read_csv(encoding_path)

	# train ML model
	train_ml_model(df, ml_model_savepath)

'''
if __name__ == '__main__':
	trainer()
'''

