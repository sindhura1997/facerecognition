Steps to Run facerecognition algorithm:
======================================

1.  Follow server installation file for Hardware and Software setup

2. 	Use Camera (Laptop of other: We used Logitech C922 Pro) for face capture
	Use script as following:
	python imgcollector.py <camera_index>
	python imgcollector 0/1 #(check using /dev/video for camera index)
	This will create faces from cmare to frames directory: While runing this file it will ask for empId/firstname/lastname. This will become you label (empId_firstname_lastname) while prediction.

3. 	Use main.py file for end to end training the model. main file will read the faces from frames directory; extract faces; create encoding and then train SVM model. 
	SVM model automatically be saved into models directory

4. 	Run fractal_face_recognition for testing the model from either using camera of video. camera or video path can be set in the config.yml file. Use below command:
	python fractal_face_recognition.py

Note:  All code has ben tested using python version3.6 (with anaconda)

