'''
# This file is  has end to end code for croping faces, encoding faces as well as training SVM model from faces.
# Command to run: python main.py ## All path and variables are set in config.yml file
'''

import os,cv2,errno,subprocess
from datetime import datetime
import yaml
from cropper import crop
from face_recognizer import trainer

with open("config.yml", 'r') as ymlfile:
	cfg = yaml.load(ymlfile)

def main():

	base_dir=os.path.dirname(os.path.realpath(__file__))
	os.chdir(cfg['FRAMES_DIR'])
	print(os.path.dirname(os.path.realpath(__file__)))
	
	os.chdir(base_dir)
	print("Cropping Starts")
	crop(cfg['FRAMES_DIR'], cfg['CROP_DIR'], cfg['dl_face_detection_path'], cfg['detection_mode'])
	print("Cropping Completed")	
	
	trainer(cfg['dl_shape_predictor_path'], cfg['dl_face_recognition_path'], cfg['CROP_DIR'], cfg['ml_model_path'], cfg['encodings_path'])

if __name__=="__main__":
	main()
