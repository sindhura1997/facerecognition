'''
# This script is used to capture and extract frame directly from the Camera
# Instruction to run:
# python imgcollector.py <camera_index - 0 or 1>
# For example: python imgcollector 0
'''

import cv2
import numpy as np
import os,sys
import time
from datetime import datetime
import errno

n_frames = 900

def get_name():
	empid = input('Please enter your employee id:\n')
	fname = input('Please enter your first name:\n')
	lname = input('Please enter your last  name:\n')
	dir_name = empid+'_'+fname+'_'+lname
	return dir_name

def make_datadir(dir_name):
	spath = os.path.realpath(__file__)
	print(spath)
	spathl = spath.split('/')
	spathl = spathl[0:-1]
	spathl.append('frames')
	spathl.append(dir_name)
	spath = '/'.join(spathl)
	try:
		os.makedirs(spath)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
		else:
			pass

	return spath

def save_data(spath):
	cap = cv2.VideoCapture(int(sys.argv[1]))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1080)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	if cap.isOpened() == False:
		print('Videocapture failed')
	#time.sleep(2)
	num = 0
	while(num<n_frames):
		ret, frame = cap.read()
		cv2.imshow('capturing...', cv2.resize(frame, (0,0), fx=0.5, fy=0.5))
		#cv2.imshow('capturing...', frame)
		if(num%8==0):
			t = str(datetime.now())
			t = t.split(' ')
			t = '_'.join(t)
			cv2.imwrite(spath+'/{}.jpg'.format(num), frame)
		num=num+1
		
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
			
	cap.release()
	cv2.destroyAllWindows()

def main():
	print('fill your name and follow instructions')
	dir_name = get_name()
	spath = make_datadir(dir_name)
	print('created dir at '+spath)
	save_data(spath)

if __name__=="__main__":


	main()
